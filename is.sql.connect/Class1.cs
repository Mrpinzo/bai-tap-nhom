﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @is.sql.connect
{
    class Connect
    {
        private bool status;
        private MySqlConnection conn;
        MySqlCommand cmd;

        public Connect(string Server, string Port, string Database, string Uid, string Pwd)
        {
            string cs = "Server=" + Server + ";Port=" + Port + ";Database=" + Database + ";Uid=" + Uid + ";Pwd=" + Pwd + ";";
            try
            {
                conn = new MySqlConnection(cs);
                cmd = new MySqlCommand();
                status = true;
            }
            catch
            {
                status = false;
            }
        }

        public Connect()
        {
            string cs = "Server=sql6.freesqldatabase.com;Port=3306;Database=sql6132674;Uid=sql6132674;Pwd=h8TaJxCg1m;";
            try
            {
                conn = new MySqlConnection(cs);
                cmd = new MySqlCommand();
                status = true;
            }
            catch
            {
                status = false;
            }
        }

        public bool getData(string Query)
        {
            cmd = new MySqlCommand(Query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (MySqlException ex)
            {
                return false;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
